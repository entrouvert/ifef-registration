# Benjamin Dauvergne <bdauvergne@entrouvert.com>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: IFEF Authentic Module 0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-01-13 22:47+0100\n"
"PO-Revision-Date: 2014-01-13 22:35+0100\n"
"Last-Translator: Benjamin Dauvergne <bdauvergne@entrouvert.com> \n"
"Language-Team: Italian\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n>1;\n"

#: ../extra/ifef.py:16 ../extra/modules/afterjobs.ptl:32
msgid "Neogia registration jobs"
msgstr ""

#: ../extra/modules/configuration.py:6
msgid "XMLRPC Neogia registration URL"
msgstr ""

#: ../extra/modules/configuration.py:11
msgid "Shared secret with the ERP"
msgstr ""

#: ../extra/modules/admin_settings.py:18
msgid "Customized stylesheet URL"
msgstr ""

#: ../extra/modules/admin_settings.py:20
msgid "The URL must be https if authentic also use https"
msgstr ""

#: ../extra/modules/admin_settings.py:22
msgid "Prefix URL of the HTTP_REFERER"
msgstr ""

#: ../extra/modules/admin_settings.py:24
msgid "Used to find which service initiated a request to Authentic"
msgstr ""

#: ../extra/modules/admin_settings.py:27
msgid "Custom theme"
msgstr ""

#: ../extra/modules/admin_settings.py:29
msgid "Theme to use when an interaction is initated by this service"
msgstr ""

#: ../extra/modules/admin_settings.py:32
msgid "Custom domain"
msgstr ""

#: ../extra/modules/admin_settings.py:34
msgid ""
"If the IdP is published on this domain, the used theme will the custom theme "
"for this service."
msgstr ""

#: ../extra/modules/admin_settings.py:37
msgid "Registration fields"
msgstr ""

#: ../extra/modules/admin_settings.py:40
msgid "List of specific fields to show on the registration page."
msgstr ""

#: ../extra/modules/root.ptl:18
msgid "Particulier-employeur"
msgstr ""

#: ../extra/modules/root.ptl:19
msgid "Salarie du Particulier-employeur"
msgstr ""

#: ../extra/modules/root.ptl:20 ../extra/modules/root.ptl:23
msgid "inscrit"
msgstr ""

#: ../extra/modules/root.ptl:21 ../extra/modules/root.ptl:24
msgid "pas inscrit en formation"
msgstr ""

#: ../extra/modules/root.ptl:22
msgid "Assistante(e) maternel(le)"
msgstr ""

#: ../extra/modules/root.ptl:25
msgid "Formateur ou directeur d'un organisme de formation"
msgstr ""

#: ../extra/modules/root.ptl:26
msgid "labellise par l'Institut-Fepem"
msgstr ""

#: ../extra/modules/root.ptl:27
msgid "souhaitant etre labellise"
msgstr ""

#: ../extra/modules/root.ptl:28
msgid "pas encore labellise"
msgstr ""

#: ../extra/modules/root.ptl:29
msgid "Formateur consultant souhaitant entrer dans la communaute"
msgstr ""

#: ../extra/modules/root.ptl:30
msgid "Salarie d'un organisme public, d'une association ou d'une organisation"
msgstr ""

#: ../extra/modules/root.ptl:31
msgid "partenaire de l'Institut-Fepem"
msgstr ""

#: ../extra/modules/root.ptl:32
msgid "souhaitant devenir partenaire"
msgstr ""

#: ../extra/modules/root.ptl:33
msgid "pas encore partenaire"
msgstr ""

#: ../extra/modules/root.ptl:34
msgid "Un professionnel de la formation"
msgstr ""

#: ../extra/modules/root.ptl:35
msgid ""
"Une personne interessee par le sujet de la professionnalisation de l'emploi "
"familial"
msgstr ""

#: ../extra/modules/root.ptl:90
msgid "Username"
msgstr "Username"

#: ../extra/modules/root.ptl:92
msgid "The identifier can only contain letters and digits."
msgstr ""

#: ../extra/modules/root.ptl:98
msgid "Password"
msgstr "Password"

#: ../extra/modules/root.ptl:100
msgid "A password will be mailed to you."
msgstr ""

#: ../extra/modules/root.ptl:112 ../extra/modules/root.ptl:134
msgid "You are"
msgstr ""

#: ../extra/modules/root.ptl:149
msgid "You must choose a class"
msgstr ""

#: ../extra/modules/root.ptl:177
msgid "I accept the terms of use"
msgstr ""

#: ../extra/modules/root.ptl:186
msgid "Submit"
msgstr "Invia"

#: ../extra/modules/root.ptl:187
msgid "Cancel"
msgstr "Annulla"

#: ../extra/modules/root.ptl:207 ../extra/modules/root.ptl:208
msgid "Registration"
msgstr ""

#: ../extra/modules/root.ptl:250
#, python-format
msgid "Log on %s"
msgstr ""

#: ../extra/modules/root.ptl:254
msgid "ERP Change Password"
msgstr ""

#: ../extra/modules/root.ptl:312
msgid "Terms of use"
msgstr ""

#: ../extra/modules/root.ptl:317
msgid "CNIL disclaimer"
msgstr ""

#: ../extra/modules/afterjobs.ptl:38
#, python-format
msgid "Run job %s"
msgstr ""

#: ../extra/modules/afterjobs.ptl:57
msgid "Id"
msgstr ""

#: ../extra/modules/afterjobs.ptl:57
msgid "Action"
msgstr ""

#: ../extra/modules/afterjobs.ptl:57
msgid "Error"
msgstr ""

#: ../extra/modules/store.py:18
msgid "IFEF Ldap Directory"
msgstr ""

#. Identification
#: ../extra/modules/store.py:21
msgid "Personal Title"
msgstr "Titolo"

#: ../extra/modules/store.py:25
msgid "Miss"
msgstr ""

#: ../extra/modules/store.py:26
msgid "Mister"
msgstr ""

#: ../extra/modules/store.py:27
msgid "Last name"
msgstr ""

#: ../extra/modules/store.py:28
msgid "First name"
msgstr ""

#: ../extra/modules/store.py:29
msgid "Language"
msgstr ""

#: ../extra/modules/store.py:31
msgid "French"
msgstr ""

#: ../extra/modules/store.py:32
msgid "Italian"
msgstr ""

#: ../extra/modules/store.py:33
msgid "English"
msgstr ""

#: ../extra/modules/store.py:34
msgid "Romanian"
msgstr ""

#: ../extra/modules/store.py:35
msgid "Latvian"
msgstr ""

#: ../extra/modules/store.py:36
msgid "Spanish"
msgstr ""

#: ../extra/modules/store.py:38
msgid "Birth date"
msgstr "Data di nascita"

#: ../extra/modules/store.py:38
msgid "jj/mm/aaaa"
msgstr ""

#: ../extra/modules/store.py:40
msgid "Birth place"
msgstr ""

#. Contact
#: ../extra/modules/store.py:42
msgid "Email"
msgstr "e-mail"

#: ../extra/modules/store.py:43
msgid "example: john@example.com"
msgstr ""

#. Addresses postales
#: ../extra/modules/store.py:49
msgid "Professional address"
msgstr ""

#: ../extra/modules/store.py:50
msgid "Professional postal code"
msgstr ""

#: ../extra/modules/store.py:51 ../extra/modules/store.py:57
msgid "Only digits."
msgstr ""

#: ../extra/modules/store.py:52
msgid "Professional city"
msgstr ""

#: ../extra/modules/store.py:53
msgid "Professional region"
msgstr ""

#: ../extra/modules/store.py:54
msgid "Professional department"
msgstr ""

#: ../extra/modules/store.py:55
msgid "Personnal address"
msgstr ""

#: ../extra/modules/store.py:56
msgid "Personnal postal code"
msgstr ""

#: ../extra/modules/store.py:58
msgid "Personnal city"
msgstr ""

#: ../extra/modules/store.py:59
msgid "Personnal region"
msgstr ""

#: ../extra/modules/store.py:60
msgid "Personnal department"
msgstr ""

#: ../extra/modules/store.py:62
msgid "User class"
msgstr ""

#. Telephone
#: ../extra/modules/store.py:67
msgid "Mobile phone"
msgstr ""

#: ../extra/modules/store.py:68 ../extra/modules/store.py:72
#: ../extra/modules/store.py:76
msgid "Only numbers, dots or spaces."
msgstr ""

#: ../extra/modules/store.py:71
msgid "Telephone perso"
msgstr ""

#: ../extra/modules/store.py:75
msgid "Telephone pro"
msgstr ""

#. Web
#: ../extra/modules/store.py:80
msgid "Twitter account"
msgstr ""

#: ../extra/modules/store.py:81
msgid "example: @twitter, identica, etc."
msgstr ""

#: ../extra/modules/store.py:82
msgid "Social network account"
msgstr ""

#: ../extra/modules/store.py:83
msgid "example: compte facebook, linkedin, viadeo, etc."
msgstr ""

#: ../extra/modules/store.py:84
msgid "Personnal WEB page"
msgstr ""

#: ../extra/modules/store.py:85
msgid "VoIP address"
msgstr ""

#: ../extra/modules/store.py:86
msgid "example: skype, sip, gtalk"
msgstr ""

#. Attributs specificiques IFEF
#: ../extra/modules/store.py:89
msgid "Pass assistant maternelle number"
msgstr ""

#: ../extra/modules/store.py:91 ../extra/modules/store.py:96
#: ../extra/modules/store.py:101 ../extra/modules/store.py:106
#: ../extra/modules/store.py:111
msgid "Only digits"
msgstr ""

#: ../extra/modules/store.py:94
msgid "Pass SPE number"
msgstr ""

#: ../extra/modules/store.py:99
msgid "URSSAF number"
msgstr ""

#: ../extra/modules/store.py:104
msgid "PAJE number"
msgstr ""

#: ../extra/modules/store.py:109
msgid "IRCEM number"
msgstr ""

#: ../extra/modules/store.py:114
msgid "Affiliated organisation's LDAP distinguished name"
msgstr ""
