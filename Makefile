# Basic makefile for project packages building and uploading
PACKAGE=`head -n1 debian/changelog | sed 's/\(^[a-z-]*\).*/\1/'`
VERSION=`head -n1 debian/changelog | sed 's/.*(\(.*\)).*/\1/'`
MAINTAINER=`grep Maintainer debian/control | sed 's/Maintainer: //'`
DISTRIBUTION=`head -n1 debian/changelog | sed 's/.* \([a-z-]*\);.*/\1/'`

.PHONY: package upload

package:
	git diff --quiet >/dev/null || false
	lenny env DEBEMAIL="\"$(MAINTAINER)\"" debchange -i -D $(DISTRIBUTION) --force-distribution $(DEBCHANGE)
	echo $(VERSION)
	read e
	git commit debian/changelog -m 'Update changelog'
	lenny debuild -I.git
	make upload

upload:
	dput lupin-$(DISTRIBUTION) ../$(PACKAGE)_$(VERSION)_*.changes
	git push origin

ifef.zip: theme/ifef/*
	cd theme; zip -r ../ifef ifef

centreressources.zip: theme/centreressources/*
	cd theme; zip -r ../centreressources centreressources

clean:
	-rm -f ifef.zip
