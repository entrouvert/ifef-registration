from authentic.schemas.schema import *
import authentic.admin.configuration as configuration

presentation_args = { 'cols': 80, 'rows': 25 }
for schema in (String('xmlrpc_registration_url',
            N_('XMLRPC Neogia registration URL'),
            default = '',
            allow_none=True,
            presentation_hint = { 'args': { 'size': 100 }}),
        String('erp_secret',
            N_('Shared secret with the ERP'),
            default = '',
            allow_none=True,
            presentation_hint = { 'args': { 'size': 100 }}),):
    configuration.identity_configuration.add_schema(schema)
