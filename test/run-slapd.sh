#!env sh

BASEDN=$1
BASELDIF=$2

if ! test "x$BASEDN" != "x"; then
	echo Missing BASEDN
	exit 1
fi

if [ ! -f "$BASELDIF" ]; then
	echo Missing LDIF file $BASELDIF
	exit 1
fi

mkdir -p tmp
SLAPD_CONF=`tempfile -d ./tmp -p slapd.conf`
SLAPD_DATA=`mktemp --tmpdir=./tmp -d slapd.data.XXXXXXX`
SLAPD_PID=`tempfile -d ./tmp -p slapd.pid`

cat >$SLAPD_CONF <<EOF
modulepath /usr/lib/ldap
moduleload back_hdb
include         /etc/ldap/schema/core.schema
include         /etc/ldap/schema/cosine.schema
include         /etc/ldap/schema/nis.schema
include         /etc/ldap/schema/inetorgperson.schema
include ../ldap/ifef.schema
# fichiers qui stockent les arguments et les PID du serveur
pidfile $SLAPD_PID
##############################################################
# ldbm database definitions
##############################################################
# Type de l'annuaire LDAP
database hdb
directory $SLAPD_DATA
# Définition de la racine du serveur
suffix ""
rootdn "cn=root,$BASEDN"
rootpw "root"
# cleartext passwords, especially for the rootdn, should
# be avoid. See slapd.conf(5) for details.
# directory /var/lib/ldap
# Définition des accès autorisés à la base LDAP
EOF

cat $SLAPD_CONF

echo /usr/sbin/slapadd -q -f $SLAPD_CONF -l $BASELDIF
/usr/sbin/slapadd -q -f $SLAPD_CONF -l $BASELDIF
echo /usr/sbin/slapd -f $SLAPD_CONF -h ldap://localhost:1389
/usr/sbin/slapd -f $SLAPD_CONF -h ldap://localhost:10389

echo Press enter to terminate
read i
kill $(cat $SLAPD_PID)
rm -rf tmp
