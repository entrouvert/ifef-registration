import qommon.template as template
from quixote import get_request, get_publisher, get_session
from qommon import get_cfg
from admin_settings import STYLESHEET_URL, REFERER_PREFIX_URL, THEME, \
        CUSTOM_DOMAIN

__old_decorate = template.decorate

def decorate(body, response):
    request = get_request()
    referer = request.environ.get('HTTP_REFERER')
    domain = request.environ.get('SERVER_NAME')
    session = get_session()

    more_css = []
    body_class = []
    theme = None
    stylesheet_url = None
    requesting_service = None

    for key, value in get_cfg('providers', {}).iteritems():
        custom_domain = value.get(CUSTOM_DOMAIN)
        referer_prefix_url = value.get(REFERER_PREFIX_URL)
        if custom_domain == domain or \
            (request.form and request.form.get('service') == key) or \
            (referer_prefix_url and referer and referer.startswith(referer_prefix_url)) or \
            getattr(session, 'service', None) == key:
            theme = value.get(THEME)
            requesting_service = key
            stylesheet_url = value.get(STYLESHEET_URL)
            break
    if requesting_service:
        session.service = requesting_service
        body_class.append(requesting_service)
        if stylesheet_url:
            more_css.append(stylesheet_url)
    if theme:
        get_publisher().cfg['branding']['theme'] = theme
    response.filter.update({'more_css': more_css, 'body_class': body_class})
    return __old_decorate(body, response)

template.decorate = decorate
import authentic.sessions
authentic.sessions.BasicSession._has_info_keys.append('service')
