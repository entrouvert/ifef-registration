import authentic.admin.settings as settings
from authentic.form import *
import qommon.template
from qommon import get_cfg
from quixote import get_publisher, redirect
import authentic.identities as identities

STYLESHEET_URL = 'stylesheet_url'
REFERER_PREFIX_URL = 'referer_prefix_url'
CUSTOM_DOMAIN = 'custom_domain'
REGISTRATION_FIELD = 'registration_fields'
THEME = 'theme'

class NewLibertyProviderUI(settings.LibertyProviderUI):
    def edit_form(self):
        form = super(NewLibertyProviderUI, self).edit_form()
        form.add(StringWidget, STYLESHEET_URL,
                title=_('Customized stylesheet URL'),
                value=self.lp.get(STYLESHEET_URL, ''),
                hint=_('The URL must be https if authentic also use https'))
        form.add(StringWidget, REFERER_PREFIX_URL,
                title=_('Prefix URL of the HTTP_REFERER'),
                value=self.lp.get(REFERER_PREFIX_URL, ''),
                hint=_('Used to find which service initiated a request to Authentic'))
        names = [None] + [x[0] for x in qommon.template.get_themes().iteritems()]
        form.add(SingleSelectWidget, THEME,
                title=_('Custom theme'),
                value=self.lp.get(THEME,None),
                hint=_('Theme to use when an interaction is initated by this service'),
                options=names)
        form.add(StringWidget, CUSTOM_DOMAIN,
                title=_('Custom domain'),
                value=self.lp.get(CUSTOM_DOMAIN,None),
                hint=_('If the IdP is published on this domain, the used theme will the custom theme for this service.'),
                options=names)
        form.add(identities.WidgetList, REGISTRATION_FIELD,
                title=_('Registration fields'),
                element_type=StringWidget,
                value=self.lp.get(REGISTRATION_FIELD,[]),
                hint=_('List of specific fields to show on the registration page.'))
        return form
    def edit_submit(self):
        return super(NewLibertyProviderUI, self).edit_submit()

class NewLibertyProvidersDir(settings.LibertyProvidersDir):
    def submit_new(self, form, key_provider_id=None):
        lpk, error = super(NewLibertyProvidersDir, self).submit_new(form,
                key_provider_id)
        if not error and form.get_widget(STYLESHEET_URL):
            v = {}
            for k in (STYLESHEET_URL, REFERER_PREFIX_URL, THEME, CUSTOM_DOMAIN, REGISTRATION_FIELD):
                v[k] = form.get_widget(k).parse()
            get_cfg('providers').get(lpk).update(v)
            get_publisher().write_cfg()
        return lpk, error

settings.LibertyProviderUI = NewLibertyProviderUI
settings.LibertyProvidersDir = NewLibertyProvidersDir
