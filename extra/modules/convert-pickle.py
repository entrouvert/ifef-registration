import pickle

import sys

pickle_file, from_codec, to_codec = sys.argv[1:]

d = pickle.load(file(pickle_file))


def conv(o):
    if isinstance(o, (tuple, list)):
        return type(o)(conv(x) for x in o)
    elif isinstance(o, dict):
        return dict((conv(k), conv(v)) for k, v in o.iteritems())
    elif isinstance(o, str):
        return o.decode(from_codec).encode(to_codec)
    else:
        return o

pickle.dump(conv(d), file(pickle_file, 'w'))
