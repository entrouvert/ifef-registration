import authentic.admin.configuration as configuration
import xmlrpclib
from qommon.storage import StorableObject
from qommon.publisher import get_logger


class XmlRpcAction(object):
    ACTION_CREATE_LDAP_CLASSIFICATION = 'IFEF-createLDAPClassification'
    ACTION_CREATE_LDAP_UPDATE = 'IFEF-createLDAPUpdate'

    TYPE_ORGANISME = 'ORGANISME'
    TYPE_PERSONNE = 'PERSONNE'
    TYPE_DELETE = 'DELETE'

    def __init__(self, action, *args, **kwargs):
        url = kwargs.get('url')
        if url is None:
            identity_configuration = configuration.get_configuration('identities')
            url = identity_configuration.get('xmlrpc_registration_url')
        self.url = url
        self.action = action
        self.args = args

    @classmethod
    def create_ldap_classification(cls, uid, email, classification, url = None):
        payload = { 'uid': uid, 'email': email, 'classification': classification}
        return XmlRpcAction(XmlRpcAction.ACTION_CREATE_LDAP_CLASSIFICATION, payload, url = url)


    @classmethod
    def create_ldap_update(cls, type, id, url = None):
        payload = { 'type': type, 'id': id }
        return XmlRpcAction(XmlRpcAction.ACTION_CREATE_LDAP_UPDATE, payload, url = url)

    def __call__(self):
        server = xmlrpclib.ServerProxy(self.url)
        method = getattr(server, self.action, None)
        if method is None:
            raise Exception('No action %s on XMLRPC endpoint %s' % (self.action, self.url))
        return method(*self.args)

    def __str__(self):
        return 'XMLRPC Call to url: %s method: %s arguments: %r' % (self.url, self.action, self.args)

class BatchJob(StorableObject):
    _names = 'batchjobs'

    def __init__(self, action):
        self.action = action
        self.error = ''
        StorableObject.__init__(self)

    def migrate(self):
        if not hasattr(self, 'error'):
            self.error = ''

    def do(self):
        try:
            result = self.action()
        except Exception, e:
            self.error = repr(e)
            self.store()
        else:
            get_logger().info('Batch job %s executed. Result: %r' % (self.action, result))
            try:
                self.remove_self()
            except OSError:
                # Never saved ? Ok
                pass

    def __call__(self, job=None):
        self.do()

    def __str__(self):
        return '<BatchJob id: %s error: %r action: %s>' % (getattr(self, 'id', None), self.error, self.action)
