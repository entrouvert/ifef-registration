from quixote import get_session
import authentic.liberty.saml2
import qommon.misc as misc

# Yeah another monkey patch
old_invoke_login = authentic.liberty.saml2.RootDirectory.invoke_login
def invoke_login(self, login, query):
    print 'my invoke login'
    get_session().service = misc.get_provider_key(login.remoteProviderId)
    return old_invoke_login(self, login, query)
authentic.liberty.saml2.RootDirectory.invoke_login = invoke_login
