#! /usr/bin/env python

import os
import distutils.core
from quixote.ptl.qx_distutils import qx_build_py

def data_tree(destdir, sourcedir):
    extensions = ['.css', '.png', '.jpeg', '.jpg', '.xml', '.html', '.js', '.ezt', '.gif']
    r = []
    for root, dirs, files in os.walk(sourcedir):
        l = [os.path.join(root, x) for x in files if os.path.splitext(x)[1] in extensions]
        r.append( (root.replace(sourcedir, destdir, 1), l) )
    return r

distutils.core.setup(
        name = 'authentic-ifef',
        version = '1.0.0',
        maintainer = 'Benjamin Dauvergne',
        maintainer_email = 'bdauvergne@entrouvert.com',
        package_dir = { 'extra': 'extra' },
        packages = ['extra', 'extra.modules'],
        cmdclass = {'build_py': qx_build_py},
        data_files = data_tree('share/authentic/themes/ifef', 'theme/ifef/') +
            data_tree('share/authentic/themes/centreressources', 'theme/centreressources/'),
    )
