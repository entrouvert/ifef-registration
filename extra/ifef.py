from qommon.publisher import get_publisher_class
from qommon.cron import CronJob
import modules.store
import modules.root
import modules.configuration
import authentic.admin.root
import modules.afterjobs
import modules.callback
import modules.admin_settings
import modules.qommon_template
import modules.authentic_saml

get_publisher_class().register_translation_domain('ifef')
authentic.admin.root.register_page('afterjobs',
        modules.afterjobs.AfterJobStatusDirectory(),
        N_('Neogia registration jobs'))

def xmlrpc_resend(publisher):
    for callback in modules.callback.BatchJob.values(ignore_errors = True):
        callback.do()

get_publisher_class().register_cronjob(CronJob(xmlrpc_resend, minutes = range(0, 60, 1)))
